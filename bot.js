const Botkit = require('botkit');

const controller = Botkit.slackbot({debug:true});
const token = process.env.SLACK_TOKEN;
const bot = controller.spawn({token: token});

bot.startRTM();

if (!process.env.SLACK_TOKEN) {
    console.log('Error: Specify SLACK_TOKEN in environment');
    process.exit(1);
}

/**
* Provide a random response from an array of strings
* @param  {Array} responses Strings to respond with
* @return {String}          Randomly chosen response
*/
const randomResponse = res => res[Math.floor(Math.random()*res.length)];

/**
 * List of scopes in which idle-chat should respond
 * @type {Array}
 */
const scopes = ['direct_message'];

/**
 * Frustratingly pointless one-dimentional communication
 * @type {Array}
 */
const basic_inanities = [
  /^h(i(y(a|o))?|ello|eya?|owdy)[.,!?]?$/i,
  /^yo[.,!?]?$/i
];
/**
 * Fight fire with fire 😠😡👹
 * @type {Array}
 */
const basic_responses = ['yo','hey','sup','what\'s up','hello','hi',':wave:'];

controller.hears(basic_inanities, scopes, (bot, message) => {
  bot.reply(message, randomResponse(basic_responses));
});

/**
 * Equally pointless but computationally more sophisticated inanities
 * @type {Array}
 */
const adv_inanities = [
  /^how (are )?you( doing?)?[.,!?']?$/i,
  /^how'?s it going?[.,!?']?$/i,
  /^what(\'s)? up[.,!?]?$/i
];

/**
 * Appropriate responses to irrelevant questions
 * @type {Array}
 */
const moods = ['aright 😒','fine','good','great','super','chipper','stellar','amazing','fantastic','well','alright',':sleeping: :zzz: :sleepy:'];

controller.hears(adv_inanities, scopes, (bot, message) => {
  bot.reply(message, randomResponse(moods));
});

/**
 * Busy? If I wasn't, I wouldn't tell you.
 * @type {Array}
 */
const busy_bodies = [
  /^(are )?you busy( right now)?( at the moment)?[.,!?]?$/i,
  /^(can i ask you )?(i have )?a question[.,!?]?$/i
];
const busy_responses = [
  " can I do for you? :tophat:",
  " do you need?",
  " can I help you with?",
  "'s up?",
  "'s the job?"
];
controller.hears(busy_bodies, scopes, (bot, message) => {
  bot.reply(message, 'What'+randomResponse(busy_responses));
});
