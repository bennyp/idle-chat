Slack bot that responds in kind when co-workers send idle chit-chat.

When co-workers send PM's like ‟Hey”, ‟Hi”, ‟Yo”, ‟What's up?” and especially "Can I ask you a question", idle-chat should respond in a believable manner with similarly inane banter.

It's important that these types of messages not distract the user. Idle-chat should mark such messages as read, only alerting the user if a non-assinine message is received.

# Running the Bot

1. `yarn install`
2. `SLACK_TOKEN={your slack token here} node bot.js`
